import * as Discord from "discord.js";
import * as isOnline from "is-online";
import * as dotenv from "dotenv";
import * as raffleCommands from "./raffleCommands";
import * as otherCommands from "./otherCommands";
import * as chatCommands from "./chatCommands";
import * as lodestoneCommands from "./lodestoneCommands";

dotenv.config();

const client: Discord.Client = new Discord.Client({
  partials: [
    Discord.Partials.Message,
    Discord.Partials.Channel,
    Discord.Partials.Reaction,
  ],
  intents: [
    Discord.GatewayIntentBits.Guilds,
    Discord.GatewayIntentBits.GuildMessages,
    Discord.GatewayIntentBits.GuildMessageReactions,
    Discord.GatewayIntentBits.MessageContent,
  ],
});

const commands = [
  {
    name: "raffle",
    description: "Draw a raffle winner! Correct usage is /raffle <message_id>",
    options: [
      {
        name: "message",
        description: "The Message ID you want to raffle from",
        required: true,
        type: 3,
      },
    ],
  },
  {
    name: "basic_raffle",
    description:
      "Draw a raffle winner! Correct usage is /quick_raffle <channel_id> <message_id>",
    options: [
      {
        name: "channel",
        description: "The Channel ID you want to raffle from",
        required: true,
        type: 3,
      },
      {
        name: "message",
        description: "The Message ID you want to raffle from",
        required: true,
        type: 3,
      },
    ],
  },
  {
    name: "quick_raffle",
    description:
      "Draw a raffle winner! Correct usage is /quick_raffle <participant1,participant2,participant3,...>",
    options: [
      {
        name: "participants",
        description: "Comma-seperated list of raffle participants",
        required: true,
        type: 3,
      },
    ],
  },
  {
    name: "meows",
    description: "Find out how many times this bot has meowed in this server!",
  },
  {
    name: "recent_winners",
    description: "Find out the four most recent raffle winners on this server!",
  },
  {
    name: "top_winners",
    description: "Find out the five most lucky raffle winners on this server!",
  },
  {
    name: "raffle_wins",
    description:
      "Find out how many times someone has won the raffle, and when their most recent win was!",
    options: [
      {
        name: "user",
        description: "The handle of the user you want to check",
        required: false,
        type: 6,
      },
    ],
  },
  {
    name: "set_raffle_channel",
    description:
      "Set the channel for this guild's raffle. Correct use is /set_raffle_channel <channel_id>",
    options: [
      {
        name: "raffle_channel",
        description: "The Channel ID you want to raffle from",
        required: true,
        type: 3,
      },
    ],
  },
  {
    name: "set_raffle_emote",
    description:
      "Set the emote for this guild's raffle. Correct use is /set_raffle_emote <emoteid>",
    options: [
      {
        name: "raffle_emote",
        description: "The emoteID you want to raffle from",
        required: true,
        type: 3,
      },
    ],
  },
  {
    name: "minecraft",
    description: "Get the information for the guild's minecraft server",
  },
  {
    name: "cat",
    description: "Get a random cat image!",
  },
  {
    name: "dog",
    description: "Get a random dog image!",
  },
  {
    name: "company_members",
    description: "Get a plaintext list of every member in the FC",
  },
];

const rest = new Discord.REST({ version: "10" }).setToken(
  process.env.DISCORDKEY!
);

(async () => {
  try {
    console.log("Started refreshing application (/) commands.");

    await rest.put(
      Discord.Routes.applicationCommands(process.env.APPLICATIONCOMMANDS!),
      {
        body: commands,
      }
    );

    console.log("Successfully reloaded application (/) commands.");
  } catch (error) {
    console.error(error);
  }
})();

client.on("ready", () => {
  console.log(`Logged in as ${client.user!.tag}!`);
});

client.on("messageCreate", async (message: Discord.Message) => {
  //Disabled due to consistent permissions issues
  /*try {
    if (message.author.bot) {
      return;
    }
    chatCommands.chatCommands(message);
  } catch (err: any) {
    console.log(err);
  }*/
});

client.on("interactionCreate", async (interaction) => {
  if (!interaction.isChatInputCommand()) return;

  if (interaction.commandName === "raffle") {
    raffleCommands.raffleCommand(interaction);
  } else if (interaction.commandName === "basic_raffle") {
    raffleCommands.basicRaffleCommand(interaction);
  } else if (interaction.commandName === "quick_raffle") {
    raffleCommands.quickRaffleCommand(interaction);
  } else if (interaction.commandName === "recent_winners") {
    raffleCommands.recentWinnersCommand(interaction);
  } else if (interaction.commandName === "top_winners") {
    raffleCommands.topWinnersCommand(interaction);
  } else if (interaction.commandName === "raffle_wins") {
    raffleCommands.userWinningsCommand(interaction);
  } else if (interaction.commandName === "meows") {
    otherCommands.meowsCommand(interaction);
  } else if (interaction.commandName === "set_raffle_channel") {
    raffleCommands.setRaffleChannel(interaction);
  } else if (interaction.commandName === "set_raffle_emote") {
    raffleCommands.setRaffleEmote(interaction);
  } else if (interaction.commandName === "minecraft") {
    otherCommands.getMinecraftServer(interaction);
  } else if (interaction.commandName === "cat") {
    otherCommands.getCatImage(interaction);
  } else if (interaction.commandName === "dog") {
    otherCommands.getDogImage(interaction);
  } else if (interaction.commandName === "company_members") {
    lodestoneCommands.companyMembers(interaction, client);
  }
});

client.login(process.env.DISCORDKEY);
