# Raffle Bot

This project is a Discord Bot, mainly for use in the FFXIV Guild 42's Discord Server. The primary function is giveaway raffles.

## Commands

    name: "raffle",
    description: "Draw a raffle winner! Correct usage is /raffle <message_id>",
   
    name: "basic_raffle",
    description:
      "Draw a raffle winner! Correct usage is /quick_raffle <channel_id> <message_id>",
    
    name: "quick_raffle",
    description:
      "Draw a raffle winner! Correct usage is /quick_raffle <participant1,participant2,participant3,...>",
    
    name: "meows",
    description: "Find out how many times this bot has meowed in this server!",

    name: "recent_winners",
    description: "Find out the four most recent raffle winners on this server!",
 
    name: "top_winners",
    description: "Find out the five most lucky raffle winners on this server!",
  
    name: "raffle_wins",
    description:
      "Find out how many times someone has won the raffle, and when their most recent win was!",
    
    name: "set_raffle_channel",
    description:
      "Set the channel for this guild's raffle. Correct use is /set_raffle_channel <channel_id>",
    
    name: "set_raffle_emote",
    description:
      "Set the emote for this guild's raffle. Correct use is /set_raffle_emote <emoteid>",
    
    name: "minecraft",
    description: "Get the information for the guild's minecraft server",
 
    name: "cat",
    description: "Get a random cat image!",
 
    name: "dog",
    description: "Get a random dog image!",
 
    name: "company_members",
    description: "Get a plaintext list of every member in the FC",
 