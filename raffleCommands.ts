import * as Discord from "discord.js";
import * as postgres from "postgres";
import * as dotenv from "dotenv";
dotenv.config();
const connectionInfo = JSON.parse(process.env.CONNECTIONINFO!);

export async function raffleCommand(
  interaction: Discord.ChatInputCommandInteraction
) {
  interaction.options.getString("message");
  await interaction.deferReply();
  try {
    const raffleID: string = interaction.options.getString("message") as string;

    const pgClient = postgres(connectionInfo!);
    const channelQuery = await pgClient`
        SELECT 
          channelid
        FROM 
          raffle_channels
        WHERE
          guild = ${interaction.guildId}`;

    if (channelQuery.length == 0) {
      const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
        .setColor(0xff0000)
        .setTitle("❌ An error occured while trying to raffle!")
        .setThumbnail(interaction.guild?.iconURL()!)
        .addFields({
          name: "Error:",
          value:
            "The raffle channel has not been set. Use the /set_raffle_channel command",
        });
      await interaction.editReply({
        embeds: [embed],
      });
      return;
    }
    const raffleChannel: Discord.TextChannel =
      (await interaction.guild?.channels.fetch(
        channelQuery[0].channelid
      )) as Discord.TextChannel;

    const raffleReactions: Discord.ReactionManager = await (
      await raffleChannel.messages.fetch(raffleID)
    ).reactions;

    const emoteQuery = await pgClient`
        SELECT 
          emote_id
        FROM 
          emote_ids
        WHERE
          guild = ${interaction.guildId}`;

    var allReactions: Discord.Collection<string, Discord.User> =
      await raffleReactions.resolve(emoteQuery[0].emote_id)?.users.fetch()!; //TODO: We can ask for this and put it in a database.

    var winnerUser: Discord.GuildMember =
      await interaction.guild?.members.fetch(allReactions.randomKey()!)!;

    var name: string | null = winnerUser.nickname;
    if (name == null) {
      name = winnerUser.displayName;
    }

    const queryResult = await pgClient`
        SELECT 
          winnerid
        FROM 
          (SELECT * FROM raffle_winners WHERE guild = ${interaction.guildId} ORDER BY date DESC LIMIT 4) sub 
        ORDER BY 
          date ASC`;
    pgClient.end();

    var validationResponse: string = "✅";

    for (let i = 0; i < queryResult.length; i++) {
      if (winnerUser.id == queryResult[i].winnerid) {
        validationResponse =
          "❌: Drawn winner has won within the last four raffles.";
      }
    }

    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x0099ff)
      .setTitle("A Raffle Winner Has Been Drawn!")
      .setThumbnail(winnerUser.displayAvatarURL()!)
      .addFields(
        {
          name: "Winner:",
          value: "The winner of this draw is: ***" + name + "***!",
        },
        {
          name: "Auto-Validation:",
          value: validationResponse,
        }
      );

    const message: Discord.Message = await interaction.editReply({
      embeds: [embed],
    });
    message.react("✅").then(() => message.react("❌"));

    const filter = (reaction: Discord.MessageReaction, user: Discord.User) => {
      return (
        ["✅", "❌"].includes(reaction.emoji.name!) &&
        user.id === interaction.user.id
      );
    };

    message
      .awaitReactions({ filter, max: 1, time: 60000, errors: ["time"] })
      .then(async (collected) => {
        const reaction = collected.first();

        if (reaction?.emoji.name === "✅") {
          //add to database
          const pgClient = postgres(connectionInfo!);
          await pgClient`
                      INSERT INTO
                        raffle_winners ( guild, winnerID, date )
                      VALUES
                        ( ${interaction.guildId}, ${
            winnerUser.id
          }, ${Date.now()} )`;

          pgClient.end();

          await message.reactions
            .removeAll()
            .catch((error) =>
              console.error("Failed to clear reactions:", error)
            );
        } else {
          //redraw
          var isValid: boolean = false;
          while (!isValid) {
            await message.reactions
              .removeAll()
              .catch((error) =>
                console.error("Failed to clear reactions:", error)
              );

            winnerUser = await interaction.guild?.members.fetch(
              allReactions.randomKey()!
            )!;
            var name: string | null = winnerUser.nickname;
            if (name == null) {
              name = winnerUser.displayName;
            }
            var validationResponse: string = "✅";

            for (let i = 0; i < queryResult.length; i++) {
              if (winnerUser.id == queryResult[i].winnerid) {
                validationResponse =
                  "❌: Drawn winner has won within the last four raffles.";
              }
            }
            const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
              .setColor(0x0099ff)
              .setTitle("A Raffle Winner Has Been Drawn!")
              .setThumbnail(winnerUser.displayAvatarURL()!)
              .addFields(
                {
                  name: "Winner:",
                  value: "The winner of this draw is: ***" + name + "***!",
                },
                {
                  name: "Auto-Validation:",
                  value: validationResponse,
                }
              );
            interaction.editReply({ embeds: [embed] });
            await message.react("✅").then(() => message.react("❌"));

            const filter = (
              reaction: Discord.MessageReaction,
              user: Discord.User
            ) => {
              return (
                ["✅", "❌"].includes(reaction.emoji.name!) &&
                user.id === interaction.user.id
              );
            };

            await message
              .awaitReactions({ filter, max: 1, time: 60000, errors: ["time"] })
              .then(async (collected) => {
                const reaction = collected.first();

                if (reaction?.emoji.name === "✅") {
                  //add to database
                  const pgClient = postgres(connectionInfo!);
                  await pgClient`
                      INSERT INTO
                        raffle_winners ( guild, winnerID, date )
                      VALUES
                        ( ${interaction.guildId}, ${
                    winnerUser.id
                  }, ${Date.now()} )`;

                  pgClient.end();

                  await message.reactions
                    .removeAll()
                    .catch((error) =>
                      console.error("Failed to clear reactions:", error)
                    );
                  isValid = true;
                } else {
                  isValid = false;
                }
              })
              .catch((collected) => {
                isValid = true;
                message.reactions
                  .removeAll()
                  .catch((error) =>
                    console.error("Failed to clear reactions:", error)
                  );
              });
          }
        }
      });
  } catch (err: any) {
    await interaction.editReply("There was a problem with this command.");
    console.log(err);
  }
}

export async function recentWinnersCommand(
  interaction: Discord.ChatInputCommandInteraction
) {
  interaction.options.getString("message");
  await interaction.deferReply();
  try {
    const pgClient = postgres(connectionInfo!);
    const queryResult = await pgClient`
      SELECT 
        *
      FROM 
        (SELECT * FROM raffle_winners WHERE guild = ${interaction.guildId} ORDER BY date DESC LIMIT 4) sub 
      ORDER BY 
        date DESC`;
    pgClient.end();

    if (queryResult.length == 0) {
      const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
        .setColor(0xff0000)
        .setTitle("❌ There are no recent winners for this server.")
        .setThumbnail(interaction.guild?.iconURL()!);
      await interaction.editReply({
        embeds: [embed],
      });
      return;
    }

    var resultString: string = "";

    for (let i = 0; i < queryResult.length; i++) {
      try {
        var winnerUser: Discord.GuildMember =
          await interaction.guild?.members.fetch(queryResult[i].winnerid)!;
        var name: string | null = winnerUser.nickname;
        if (name == null) {
          name = winnerUser.displayName;
        }
      } catch {
        name = "Unknown Member";
      }

      var date: Date = queryResult[i].date;
      resultString += `${i + 1}: ***${name}*** ${
        date.getMonth() + 1
      }/${date.getDate()}/${date.getFullYear()}\n`;
    }

    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x0099ff)
      .setTitle(`Recent winners in ${interaction.guild?.name}!`)
      .setThumbnail(interaction.guild?.iconURL()!)
      .addFields({
        name: "Winners:",
        value: resultString,
      });
    interaction.editReply({ embeds: [embed] });
  } catch (err: any) {
    await interaction.editReply("There was a problem with this command.");
    console.log(err);
  }
}

export async function topWinnersCommand(
  interaction: Discord.ChatInputCommandInteraction
) {
  interaction.options.getString("message");
  await interaction.deferReply();
  try {
    const pgClient = postgres(connectionInfo!);
    const queryResult = await pgClient`
      SELECT 
        COUNT(w.winnerid), w.winnerid 
      FROM 
        raffle_winners w 
      WHERE 
        guild = ${interaction.guildId} 
      GROUP BY 
        w.winnerid, w.guild 
      ORDER BY 
        count(w.winnerid) 
      DESC LIMIT 5;`;
    pgClient.end();

    if (queryResult.length == 0) {
      const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
        .setColor(0xff0000)
        .setTitle("❌ There are no recent winners for this server.")
        .setThumbnail(interaction.guild?.iconURL()!);
      await interaction.editReply({
        embeds: [embed],
      });
      return;
    }

    var resultString: string = "";

    for (let i = 0; i < queryResult.length; i++) {
      try {
        var winnerUser: Discord.GuildMember =
          await interaction.guild?.members.fetch(queryResult[i].winnerid)!;
        var name: string | null = winnerUser.nickname;
        if (name == null) {
          name = winnerUser.displayName;
        }
      } catch {
        name = "Unknown Member";
      }

      var date: Date = queryResult[i].date;
      resultString += `${i + 1}: ***${name}*** ${queryResult[i].count}\n`;
    }

    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x0099ff)
      .setTitle(`Top winners in ${interaction.guild?.name}!`)
      .setThumbnail(interaction.guild?.iconURL()!)
      .addFields({
        name: "Winners:",
        value: resultString,
      });
    interaction.editReply({ embeds: [embed] });
  } catch (err: any) {
    await interaction.editReply("There was a problem with this command.\n");
    console.log(err);
  }
}

export async function userWinningsCommand(
  interaction: Discord.ChatInputCommandInteraction
) {
  interaction.options.getString("message");
  await interaction.deferReply();
  var userId: string = interaction.user.id;
  if (interaction.options.getUser("user") != null) {
    userId = interaction.options.getUser("user").id;
  }
  try {
    const pgClient = postgres(connectionInfo!);
    const queryResult = await pgClient`
      SELECT 
        date
      FROM 
        raffle_winners
      WHERE
        winnerID = ${userId} 
      AND
        guild = ${interaction.guildId}`;

    pgClient.end();

    if (queryResult.length == 0) {
      const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
        .setColor(0xff0000)
        .setTitle("❌ This user has not won a raffle in this server!");
      await interaction.editReply({
        embeds: [embed],
      });
      return;
    }

    var user: Discord.GuildMember = await interaction.guild?.members.fetch(
      userId
    )!;
    var name: string | null = user.nickname;
    if (name == null) {
      name = user.displayName;
    }
    const date: Date = queryResult[queryResult.length - 1].date;
    const resultString: string = `${
      date.getMonth() + 1
    }/${date.getDate()}/${date.getFullYear()}\n`;

    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x0099ff)
      .setTitle(`Recent raffle wins for ${name} in ${interaction.guild?.name}!`)
      .setThumbnail(user.displayAvatarURL()!)
      .addFields(
        {
          name: "Most Recent Win:",
          value: resultString,
        },
        {
          name: "Total Wins:",
          value: queryResult.length.toString(),
        }
      );
    interaction.editReply({ embeds: [embed] });
  } catch (err: any) {
    await interaction.editReply("There was a problem with this command.");
    console.log(err);
  }
}

export async function setRaffleChannel(
  interaction: Discord.ChatInputCommandInteraction
) {
  try {
    const channelID: string = interaction.options.getString(
      "raffle_channel"
    ) as string;
    const pgClient = postgres(connectionInfo!);
    const queryResult = await pgClient`
                select
                  COUNT(guild)
                from raffle_channels
                WHERE 
                  guild = ${interaction.guildId}`;
    if (queryResult[0].count == 0) {
      await pgClient`
              INSERT INTO
                raffle_channels ( guild, channelid )
              VALUES
                ( ${interaction.guildId}, ${channelID} )`;
    } else {
      await pgClient`
                UPDATE
                  raffle_channels
                SET 
                channelid = ${channelID}
                WHERE
                  guild = ${interaction.guildId}`;
    }

    pgClient.end();
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x00ff00)
      .setTitle("✅ Successfully set raffle channel!");
    await interaction.reply({
      embeds: [embed],
    });
  } catch {
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0xff0000)
      .setTitle("❌ An error occured while trying to set the raffle channel!!")
      .addFields({
        name: "Error:",
        value:
          "An error occured. Verify the validity of the channel and use the /set_raffle_channel command again",
      });
    await interaction.reply({
      embeds: [embed],
    });
  }
}

export async function basicRaffleCommand(
  interaction: Discord.ChatInputCommandInteraction
) {
  interaction.options.getString("message");
  await interaction.deferReply();
  try {
    const raffleID: string = interaction.options.getString("message") as string;

    const raffleChannel: Discord.TextChannel =
      (await interaction.guild?.channels.fetch(
        interaction.options.getString("channel") as string
      )) as Discord.TextChannel;

    const raffleReactions: Discord.ReactionManager = await (
      await raffleChannel.messages.fetch(raffleID)
    ).reactions;

    var allReactions: Discord.Collection<string, Discord.User> =
      await raffleReactions.resolve("787123807323684934")?.users.fetch()!; //TODO: We can ask for this and put it in a database.

    var winnerUser: Discord.GuildMember =
      await interaction.guild?.members.fetch(allReactions.randomKey()!)!;

    var name: string | null = winnerUser.nickname;
    if (name == null) {
      name = winnerUser.displayName;
    }

    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x0099ff)
      .setTitle("A Raffle Winner Has Been Drawn!")
      .setThumbnail(winnerUser.displayAvatarURL()!)
      .addFields({
        name: "Winner:",
        value: "The winner of this draw is: ***" + name + "***!",
      });

    const message: Discord.Message = await interaction.editReply({
      embeds: [embed],
    });
    message.react("✅").then(() => message.react("❌"));

    const filter = (reaction: Discord.MessageReaction, user: Discord.User) => {
      return (
        ["✅", "❌"].includes(reaction.emoji.name!) &&
        user.id === interaction.user.id
      );
    };

    message
      .awaitReactions({ filter, max: 1, time: 60000, errors: ["time"] })
      .then(async (collected) => {
        const reaction = collected.first();

        if (reaction?.emoji.name === "✅") {
          await message.reactions
            .removeAll()
            .catch((error) =>
              console.error("Failed to clear reactions:", error)
            );
        } else {
          //redraw
          var isValid: boolean = false;
          while (!isValid) {
            await message.reactions
              .removeAll()
              .catch((error) =>
                console.error("Failed to clear reactions:", error)
              );

            winnerUser = await interaction.guild?.members.fetch(
              allReactions.randomKey()!
            )!;
            var name: string | null = winnerUser.nickname;
            if (name == null) {
              name = winnerUser.displayName;
            }

            const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
              .setColor(0x0099ff)
              .setTitle("A Raffle Winner Has Been Drawn!")
              .setThumbnail(winnerUser.displayAvatarURL()!)
              .addFields({
                name: "Winner:",
                value: "The winner of this draw is: ***" + name + "***!",
              });
            interaction.editReply({ embeds: [embed] });
            await message.react("✅").then(() => message.react("❌"));

            const filter = (
              reaction: Discord.MessageReaction,
              user: Discord.User
            ) => {
              return (
                ["✅", "❌"].includes(reaction.emoji.name!) &&
                user.id === interaction.user.id
              );
            };

            await message
              .awaitReactions({ filter, max: 1, time: 60000, errors: ["time"] })
              .then(async (collected) => {
                const reaction = collected.first();

                if (reaction?.emoji.name === "✅") {
                  //add to database
                  await message.reactions
                    .removeAll()
                    .catch((error) =>
                      console.error("Failed to clear reactions:", error)
                    );
                  isValid = true;
                } else {
                  isValid = false;
                }
              })
              .catch((collected) => {
                isValid = true;
                message.reactions
                  .removeAll()
                  .catch((error) =>
                    console.error("Failed to clear reactions:", error)
                  );
              });
          }
        }
      });
  } catch (err: any) {
    await interaction.editReply("There was a problem with this command.");
    console.log(err);
  }
}

export async function quickRaffleCommand(
  interaction: Discord.ChatInputCommandInteraction
) {
  interaction.options.getString("message");
  await interaction.deferReply();
  try {
    const raffleArray: Array<string> = (
      interaction.options.getString("participants") as string
    ).split(",");

    const name: string = raffleArray[(raffleArray.length * Math.random()) | 0];

    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x0099ff)
      .setTitle("A Raffle Winner Has Been Drawn!")
      .addFields({
        name: "Winner:",
        value: "The winner of this draw is: ***" + name + "***!",
      });

    const message: Discord.Message = await interaction.editReply({
      embeds: [embed],
    });
  } catch (err: any) {
    await interaction.editReply("There was a problem with this command.");
    console.log(err);
  }
}

export async function setRaffleEmote(
  interaction: Discord.ChatInputCommandInteraction
) {
  try {
    var emoteID: string = interaction.options.getString(
      "raffle_emote"
    ) as string;

    emoteID = emoteID.split(":")[2].replace(/[^a-zA-Z0-9]/g, "");

    const pgClient = postgres(connectionInfo!);
    const queryResult = await pgClient`
                select
                  COUNT(guild)
                from emote_ids
                WHERE 
                  guild = ${interaction.guildId}`;
    if (queryResult[0].count == 0) {
      await pgClient`
              INSERT INTO
                emote_ids ( guild, emote_id )
              VALUES
                ( ${interaction.guildId}, ${emoteID} )`;
    } else {
      await pgClient`
                UPDATE
                  emote_ids
                SET 
                  emote_id = ${emoteID}
                WHERE
                  guild = ${interaction.guildId}`;
    }

    pgClient.end();
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x00ff00)
      .setTitle("✅ Successfully set raffle emote!");
    await interaction.reply({
      embeds: [embed],
    });
  } catch {
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0xff0000)
      .setTitle("❌ An error occured while trying to set the raffle emote!")
      .addFields({
        name: "Error:",
        value:
          "An error occured. Verify the validity of the emote and use the /set_raffle_emote command again",
      });
    await interaction.reply({
      embeds: [embed],
    });
  }
}
