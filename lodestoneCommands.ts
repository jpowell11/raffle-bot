import * as Discord from "discord.js";
import * as dotenv from "dotenv";

dotenv.config();

export async function companyMembers(
  interaction: Discord.ChatInputCommandInteraction,
  client: Discord.Client
) {
  await interaction.deferReply();
  try {
    var company_data = await fetch(
      `http://localhost:8080/freecompany/9233927348481539497?data=FCM`,
      { mode: "cors" } //hardcoded for now
    ).then((response) => response.json());

    var names: Array<string> = [];
    var currentPage: number = 1;

    while (
      currentPage <= company_data.FreeCompanyMembers.Pagination.PageTotal
    ) {
      var companyMembers = company_data.FreeCompanyMembers.List;

      companyMembers.forEach((element) => {
        names.push(element.Name);
      });
      currentPage += 1;
      company_data = await fetch(
        `http://localhost:8080/freecompany/9233927348481539497?data=FCM&page=${currentPage}`,
        { mode: "cors" } //hardcoded for now
      ).then((response) => response.json());
    }

    console.dir(names, { maxArrayLength: null });

    var unixtime = Math.round(+new Date() / 1000);
    var date = new Date();
    date.setTime(unixtime * 1000);

    var content =
      `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}\n` +
      `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}\n` +
      names.length +
      "\n";

    names.sort();

    names.forEach((element) => {
      content += element + "\n";
    });

    const names_buffer = Buffer.from(content, "utf-8");
    const attachment: Discord.AttachmentBuilder = new Discord.AttachmentBuilder(
      names_buffer,
      { name: "CompanyMembers.txt" }
    );

    const user: Discord.User = client.users.cache.get(
      interaction.member.user.id
    );
    user.send({
      files: [attachment],
    });

    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x0099ff)
      .setTitle(`Success! A list has been directly messaged to you.`);
    interaction.editReply({ embeds: [embed] });
  } catch (err) {
    console.log(err.message);
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0xff0000)
      .setTitle("❌ An error occured while trying to use this command")
      .addFields({
        name: "Error:",
        value: "An error occured.",
      });
    await interaction.editReply({
      embeds: [embed],
    });
  }
}
