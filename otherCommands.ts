import * as Discord from "discord.js";
import * as postgres from "postgres";
import * as dotenv from "dotenv";
dotenv.config();
const connectionInfo = JSON.parse(process.env.CONNECTIONINFO!);

export async function meowsCommand(
  interaction: Discord.ChatInputCommandInteraction
) {
  interaction.options.getString("message");
  try {
    const pgClient = postgres(connectionInfo!);
    const queryResult = await pgClient`
            select
              meowcount
            from meows
            WHERE 
              guild = ${interaction.guildId}`;

    pgClient.end();

    await interaction.reply(
      "I have meowed " + queryResult[0].meowcount + " times!"
    ); //TODO: Embed
  } catch (err: any) {
    await interaction.reply("There was a problem with this command.");
    console.log(err.message);
  }
}

export async function getMinecraftServer(
  interaction: Discord.ChatInputCommandInteraction
) {
  try {
    const pgClient = postgres(connectionInfo!);
    const queryResult = await pgClient`
                  select
                    serverip, maplink
                  from guild_minecraft
                  WHERE 
                    guild = ${interaction.guildId}`;

    pgClient.end();
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0x0099ff)
      .setTitle(`Minecraft Server Information for ${interaction.guild?.name}!`)
      .setThumbnail(interaction.guild?.iconURL()!)
      .addFields(
        {
          name: "Server IP:",
          value: queryResult[0].serverip,
        },
        {
          name: "Ineractive Map Link:",
          value: queryResult[0].maplink,
        }
      );
    interaction.reply({ embeds: [embed] });
  } catch {
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0xff0000)
      .setTitle("❌ An error occured while trying to use this command")
      .addFields({
        name: "Error:",
        value: "An error occured.",
      });
    await interaction.reply({
      embeds: [embed],
    });
  }
}

export async function getCatImage(
  interaction: Discord.ChatInputCommandInteraction
) {
  await interaction.deferReply();
  try {
    await fetch(`https://api.thecatapi.com/v1/images/search?size=full`)
      .then((response: Response) => response.json())
      .then((json: any) => {
        const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
          .setColor(0x0099ff)
          .setTitle(`A cat for you!`)
          .setImage(json[0].url);
        interaction.editReply({ embeds: [embed] });
      });
  } catch {
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0xff0000)
      .setTitle("❌ An error occured while trying to use this command")
      .addFields({
        name: "Error:",
        value: "An error occured.",
      });
    await interaction.editReply({
      embeds: [embed],
    });
  }
}

export async function getDogImage(
  interaction: Discord.ChatInputCommandInteraction
) {
  await interaction.deferReply();
  try {
    await fetch(`https://api.thedogapi.com/v1/images/search?size=full`)
      .then((response: Response) => response.json())
      .then((json: any) => {
        const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
          .setColor(0x0099ff)
          .setTitle(`A dog for you!`)
          .setImage(json[0].url);
        interaction.editReply({ embeds: [embed] });
      });
  } catch {
    const embed: Discord.EmbedBuilder = new Discord.EmbedBuilder()
      .setColor(0xff0000)
      .setTitle("❌ An error occured while trying to use this command")
      .addFields({
        name: "Error:",
        value: "An error occured.",
      });
    await interaction.editReply({
      embeds: [embed],
    });
  }
}
