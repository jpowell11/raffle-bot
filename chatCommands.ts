import * as Discord from "discord.js";
import * as postgres from "postgres";
import * as dotenv from "dotenv";
dotenv.config();
const connectionInfo = JSON.parse(process.env.CONNECTIONINFO!);

export async function chatCommands(message: Discord.Message) {
  if (message.content.toLowerCase().includes("meow")) {
    var randNum: number = Math.random() * 15;
    const meowArray: string[] = message.content.split("meow");
    var response_string: string = "";
    const pgClient = postgres(connectionInfo!);
    const queryResult = await pgClient`
        select
          COUNT(guild)
        from meows
        WHERE 
          guild = ${message.guildId}`;
    if (queryResult[0].count == 0) {
      await pgClient`
        INSERT INTO
          meows ( guild, meowcount )
        VALUES
          ( ${message.guildId}, ${meowArray.length - 1} )`;
    } else {
      await pgClient`
        UPDATE
          meows 
        SET 
          meowcount = meowcount + ${meowArray.length - 1}
        WHERE
          guild = ${message.guildId}`;
    }

    pgClient.end();

    if (meowArray.length >= 5 || randNum == 1) {
      response_string =
        "<:em_pet:770129037997244416> the <@" +
        message.author.id +
        "> kitty. So soft. So floof";
    } else if (meowArray.length == 1) {
      var response_string: string = "meow";
    } else {
      for (var i = 1; i < meowArray.length; i++) {
        if (response_string == "") {
          response_string += "Meow";
        } else {
          response_string += " meow";
        }
      }
    }
    console.log(response_string);
    await message.reply(response_string + "!");
  } else if (message.content.toLowerCase().includes("purr")) {
    var randNum: number = Math.random() * 15;
    const meowArray: string[] = message.content.split("purr");
    var response_string: string = "";

    if (meowArray.length >= 5 || randNum == 1) {
      response_string =
        "<:em_pet:770129037997244416> the <@" +
        message.author.id +
        "> kitty. So soft. So floof.";
    } else {
      for (var i = 1; i < meowArray.length; i++) {
        if (response_string == "") {
          response_string += "purr";
        } else {
          response_string += " purr";
        }
      }
    }
    console.log(response_string);
    await message.reply(response_string);
  } else if (
    message.content.toLowerCase().includes("lizzer") ||
    message.content.toLowerCase().includes("lizz3r") ||
    message.content.toLowerCase().includes("lizzard")
  ) {
    var response_string: string = 'The correct term is "Dwagon!"';
    console.log(response_string);
    await message.reply(response_string);
  } else if (
    message.content.toLowerCase().includes("dragon") ||
    message.content.toLowerCase().includes("dwagon")
  ) {
    var response_string: string = "Thank you for using the correct term!";
    console.log(response_string);
    await message.reply(response_string);
  } else if (message.content.toLowerCase().includes("pet")) {
    var response_string: string = "<:em_pet:770129037997244416>";
    console.log(response_string);
    await message.reply(response_string);
  } else if (message.content.toLowerCase().includes("hiss")) {
    var response_string: string = `*Throws catnip at the <@${message.author.id}> kitty*`;
    console.log(response_string);
    await message.reply(response_string);
  }
}
